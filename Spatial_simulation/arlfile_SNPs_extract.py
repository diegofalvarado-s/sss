#!/usr/bin/env python
'''
script to read arlequin input files and extract the SNP sequences
Usage: python ./arlfile_SNPs_extract.py -p 7 -d ./GeneticsOutputs
Diego F. Alvarado-S., March 6, 2014
'''

import os, numpy as np
from optparse import OptionParser
usage = "usage: %prog [options] arg"
parser = OptionParser(usage=usage)
parser.add_option("-p",dest="pops", help="number of sampled populations")
parser.add_option("-d",dest="workdir", help="path where the output should be stored")
parser.add_option("--ploidy",dest="Ploidy", default= '2n', help="ploidy of dataset being analyzed")
(options,args) = parser.parse_args()

npops = int(options.pops)   #defines the number of sampled populations
path = options.workdir   #defines the path where the output should be stored
ploid = options.Ploidy

for filename in os.listdir(path):
    if '.arp' in filename:
        outfilename = '%s/%s.snp' %(path, filename.split('.')[0])
        outfile = open(outfilename, 'w')
        infilename = '%s/%s' %(path, filename)
        infile = open(infilename, 'r')
        for line in infile:
            for x in [str(y)+'_' for y in (range(npops+1)[1:])]:
                if line.startswith(x):
                    diploid_snps = np.empty([1,(len(line.strip().split('\t'))-2)])
                    row = line.strip().split('\t')
                    if ploid == '2n':
                        begin = row[0] + '\t' + row[1]
                        diploid_snps = np.vstack((diploid_snps, [int(z) for z in row[2:]]))
                        diploid_snps = np.vstack((diploid_snps, [int(z) for z in next(infile).strip().split('\t')]))
                        diploid_snps = np.delete(diploid_snps,0,0)
                        combined_snps = [str(int(z)) for z in np.sum(diploid_snps, axis=0).tolist()]
                        print >> outfile, '%s\t%s' %(begin, '\t'.join(combined_snps))
                    elif ploid == 'n':
                        print >> outfile, line.strip()
        
        infile.close()
        outfile.close()
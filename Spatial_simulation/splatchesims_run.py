#!/usr/bin/env python

'''
this script runs splatche simulations and calculates summary stats using ArlSumStat
Usage: python ./splatchesims_run.py -M M1 -s 10 --rD 10 --rG 10 --N 0
Diego F. Alvarado-S.
Mar 16, 2014
'''

import sys, os, time, numpy, subprocess, shlex
from collections import deque
from optparse import OptionParser
cwd = os.getcwd()	#gets current directory
sys.path.append(cwd)
from Arrival_time_reader import arrivalreader
worlds = ['pulse','nopulse']

usage = "usage: %prog [options] arg"
parser = OptionParser(usage=usage)
parser.add_option("-M",dest="Model", help="type of geographic scenario; M1 for 'Up-Down mountain model' or M2 for 'South-North model'")
parser.add_option("-s",dest="scenarios", help="number of scenarios to run (i.e., number of settings file pairs)")
parser.add_option("--rD",dest="Demoreps", default=10, help="number of demographic replicates per scenario")
parser.add_option("--rG",dest="Genreps", default=10, help="number of genetic replicates per scenario")
parser.add_option("--N", dest="StartNum", default = 0,help="start integer for numbering simulations output")
(options,args) = parser.parse_args() 

Model = options.Model		#defines whether a "Up-Down mountain model" (named M1) or "South-North model" (named M2) is to be run
scens = int(options.scenarios)	#defines the number of scenarios to run (i.e., number of settings file pairs)
repsDem = int(options.Demoreps)		#defines the number of demographic replicates per scenario
repsGen = int(options.Genreps)	#defines the number of genetic replicates per scenario
start = int(options.StartNum)	#defines the start integer for numbering simulations output

ascii_centerin = '%s/general_dataset/suitabilities_world_state1_%s.txt' %(cwd,Model)
ascii_centerout = '%s/general_dataset/suitabilities_world_state2_%s.txt' %(cwd,Model)
splatchefile = '%s/splatche_settings_template.txt' %(cwd)
samplefile = '%s/general_dataset/GeneSamples_%s.sam' %(cwd, Model)
Npops = int([line for line in open(samplefile, 'r')][0].strip())

parfilename = '%s/GeneticsOutputs/parameters_used_StartGroup%d.txt' %(cwd,start)
par = open(parfilename, 'w')
friction_dict = {0:0.99, 1:0.95, 2:0.90, 3:0.85, 4:0.80, 5:0.75, 6:0.70, 7:0.65, 8:0.60, 9:0.55, 10:0.50, 11:0.45, 12:0.40, 13:0.35, 14:0.30, 15:0.25, 16:0.20, 17:0.15, 18:0.10, 19:0.05, 20:0.01}
print >> par, 'WorldType\tSetReplicate\tDemoReplicate\tGrowthRate\tMigrationRate\tMaxCarryingCapacity\tMinArrivalTime\tMaxArrivalTime\tMedianArrivalTime'

for z in xrange(scens):

	#this portion creates the names of the suit2K and suit2F files for the pulse and non-pulse models
	K_in_file = '%s/general_dataset/suit2K-INT_StartGroup%d.txt' %(cwd,start)
	F_in_file = '%s/general_dataset/suit2F-INT_StartGroup%d.txt' %(cwd,start)
	K_out_file = '%s/general_dataset/suit2K-GLA_StartGroup%d.txt' %(cwd,start)
	F_out_file = '%s/general_dataset/suit2F-GLA_StartGroup%d.txt' %(cwd,start)

	suit_INT = open(ascii_centerin, 'r')
	suitsINT = []
	for suitI in suit_INT:
		suitsINT.append(suitI.strip())
	suit_INT.close()
	suit_GLA = open(ascii_centerout, 'r')
	suitsGLA = []
	for suitG in suit_GLA:
		suitsGLA.append(suitG.strip())
	suit_GLA.close()

	maxK = int(numpy.random.uniform(100,1000))	#max K
	K_vals = range(1,(maxK+(maxK/20)),(maxK/20))[0:21]
	K_vals_dict = {}
	for i in xrange(21):
		K_vals_dict[i] = K_vals[i]
		
	rate_change = [0.0, 0.25, 0.5, 0.75, 1.0]
	for rate in rate_change:
		K_transition_file = '%s/general_dataset/suit2K-%dGLA-%dINT_StartGroup%d.txt' %(cwd,int(rate*100),int((1-rate)*100),start)
		K_table = open(K_transition_file, 'w')
		F_transition_file = '%s/general_dataset/suit2F-%dGLA-%dINT_StartGroup%d.txt' %(cwd,int(rate*100),int((1-rate)*100),start)
		F_table = open(F_transition_file, 'w')
		k = 0
		for S in xrange(len(suitsINT)):
			k +=1
			if suitsINT[S] == 'NA':
				pass
			else:
				cellvalue = int((float(suitsGLA[S])*rate) + (float(suitsINT[S])*(1-rate))) 
				print >> K_table, '%d\t%d\tcell-%d' %(k, (K_vals_dict.get(cellvalue)), k)
				print >> F_table, '%d\t%.2f\tcell-%d' %(k, (friction_dict.get(cellvalue)), k)
		K_table.close()
		F_table.close()
	
	#this portion creates the paired splatche settings files for each replicate	
	GR = numpy.random.uniform(0.1,0.4)	#growth rate picked up from a uniform distribution with lower and upper limit
	m = numpy.random.uniform(0.01,0.25)	#migration rate
	par_used = 'test%d\t%.5f\t%.5f\t%d'	%((z+start),GR,m,maxK)

	for w in worlds:
		
		#this portion creates the dynamic and arrival files for each set required by splatche
		if z == 0:
			if w == 'nopulse':
				nameKnp = '%s/general_dataset/dynamic_K_%s_StartGroup%d.txt' %(cwd,w,start)
				dynKnp = open(nameKnp, 'w')
				print >> dynKnp, '5\n0 ./general_dataset/suit2K-100GLA-0INT_StartGroup%d.txt Time0' %start
				print >> dynKnp, '225 ./general_dataset/suit2K-75GLA-25INT_StartGroup%d.txt Time1' %start
				print >> dynKnp, '450 ./general_dataset/suit2K-50GLA-50INT_StartGroup%d.txt Time2' %start
				print >> dynKnp, '675 ./general_dataset/suit2K-25GLA-75INT_StartGroup%d.txt Time3' %start
				print >> dynKnp, '900 ./general_dataset/suit2K-0GLA-100INT_StartGroup%d.txt Time4' %start   #1st completed change
				dynKnp.close()
				nameFnp = '%s/general_dataset/dynamic_F_%s_StartGroup%d.txt' %(cwd,w,start)
				dynFnp = open(nameFnp, 'w')
				print >> dynFnp, '5\n0 ./general_dataset/suit2F-100GLA-0INT_StartGroup%d.txt Time0' %start
				print >> dynFnp, '225 ./general_dataset/suit2F-75GLA-25INT_StartGroup%d.txt Time1' %start
				print >> dynFnp, '450 ./general_dataset/suit2F-50GLA-50INT_StartGroup%d.txt Time2' %start
				print >> dynFnp, '675 ./general_dataset/suit2F-25GLA-75INT_StartGroup%d.txt Time3' %start
				print >> dynFnp, '900 ./general_dataset/suit2F-0GLA-100INT_StartGroup%d.txt Time4' %start   #1st completed change
				dynFnp.close()
	
			elif w == 'pulse':
				nameKp = '%s/general_dataset/dynamic_K_%s_StartGroup%d.txt' %(cwd,w,start)
				dynKp = open(nameKp, 'w')
				print >> dynKp, '29\n0 ./general_dataset/suit2K-100GLA-0INT_StartGroup%d.txt Time0' %start	
				print >> dynKp, '50 ./general_dataset/suit2K-75GLA-25INT_StartGroup%d.txt Time1' %start
				print >> dynKp, '100 ./general_dataset/suit2K-50GLA-50INT_StartGroup%d.txt Time2' %start
				print >> dynKp, '150 ./general_dataset/suit2K-25GLA-75INT_StartGroup%d.txt Time3' %start
				print >> dynKp, '200 ./general_dataset/suit2K-0GLA-100INT_StartGroup%d.txt Time4' %start   #1st completed change
				print >> dynKp, '260 ./general_dataset/suit2K-25GLA-75INT_StartGroup%d.txt Time5' %start
				print >> dynKp, '320 ./general_dataset/suit2K-50GLA-50INT_StartGroup%d.txt Time6' %start
				print >> dynKp, '380 ./general_dataset/suit2K-75GLA-25INT_StartGroup%d.txt Time7' %start
				print >> dynKp, '450 ./general_dataset/suit2K-100GLA-0INT_StartGroup%d.txt Time8' %start   #2nd completed change
				print >> dynKp, '490 ./general_dataset/suit2K-75GLA-25INT_StartGroup%d.txt Time9' %start
				print >> dynKp, '530 ./general_dataset/suit2K-50GLA-50INT_StartGroup%d.txt Time10' %start
				print >> dynKp, '570 ./general_dataset/suit2K-25GLA-75INT_StartGroup%d.txt Time11' %start
				print >> dynKp, '600 ./general_dataset/suit2K-0GLA-100INT_StartGroup%d.txt Time12' %start   #3rd completed change
				print >> dynKp, '660 ./general_dataset/suit2K-25GLA-75INT_StartGroup%d.txt Time13' %start
				print >> dynKp, '720 ./general_dataset/suit2K-50GLA-50INT_StartGroup%d.txt Time14' %start
				print >> dynKp, '780 ./general_dataset/suit2K-75GLA-25INT_StartGroup%d.txt Time15' %start
				print >> dynKp, '850 ./general_dataset/suit2K-100GLA-0INT_StartGroup%d.txt Time16' %start   #4th completed change
				print >> dynKp, '925 ./general_dataset/suit2K-75GLA-25INT_StartGroup%d.txt Time17' %start
				print >> dynKp, '1000 ./general_dataset/suit2K-50GLA-50INT_StartGroup%d.txt Time18' %start
				print >> dynKp, '1075 ./general_dataset/suit2K-25GLA-75INT_StartGroup%d.txt Time19' %start
				print >> dynKp, '1150 ./general_dataset/suit2K-0GLA-100INT_StartGroup%d.txt Time20' %start   #5th completed change
				print >> dynKp, '1240 ./general_dataset/suit2K-25GLA-75INT_StartGroup%d.txt Time21' %start
				print >> dynKp, '1330 ./general_dataset/suit2K-50GLA-50INT_StartGroup%d.txt Time22' %start
				print >> dynKp, '1420 ./general_dataset/suit2K-75GLA-25INT_StartGroup%d.txt Time23' %start
				print >> dynKp, '1500 ./general_dataset/suit2K-100GLA-0INT_StartGroup%d.txt Time24' %start   #6th completed change
				print >> dynKp, '1560 ./general_dataset/suit2K-75GLA-25INT_StartGroup%d.txt Time25' %start
				print >> dynKp, '1620 ./general_dataset/suit2K-50GLA-50INT_StartGroup%d.txt Time26' %start
				print >> dynKp, '1680 ./general_dataset/suit2K-25GLA-75INT_StartGroup%d.txt Time27' %start
				print >> dynKp, '1750 ./general_dataset/suit2K-0GLA-100INT_StartGroup%d.txt Time28' %start   #7th completed change
				dynKp.close()
				nameFp = '%s/general_dataset/dynamic_F_%s_StartGroup%d.txt' %(cwd,w,start)
				dynFp = open(nameFp, 'w')
				print >> dynFp, '29\n0 ./general_dataset/suit2F-100GLA-0INT_StartGroup%d.txt Time0' %start	
				print >> dynFp, '50 ./general_dataset/suit2F-75GLA-25INT_StartGroup%d.txt Time1' %start
				print >> dynFp, '100 ./general_dataset/suit2F-50GLA-50INT_StartGroup%d.txt Time2' %start
				print >> dynFp, '150 ./general_dataset/suit2F-25GLA-75INT_StartGroup%d.txt Time3' %start
				print >> dynFp, '200 ./general_dataset/suit2F-0GLA-100INT_StartGroup%d.txt Time4' %start   #1st completed change
				print >> dynFp, '260 ./general_dataset/suit2F-25GLA-75INT_StartGroup%d.txt Time5' %start
				print >> dynFp, '320 ./general_dataset/suit2F-50GLA-50INT_StartGroup%d.txt Time6' %start
				print >> dynFp, '380 ./general_dataset/suit2F-75GLA-25INT_StartGroup%d.txt Time7' %start
				print >> dynFp, '450 ./general_dataset/suit2F-100GLA-0INT_StartGroup%d.txt Time8' %start   #2nd completed change
				print >> dynFp, '490 ./general_dataset/suit2F-75GLA-25INT_StartGroup%d.txt Time9' %start
				print >> dynFp, '530 ./general_dataset/suit2F-50GLA-50INT_StartGroup%d.txt Time10' %start
				print >> dynFp, '570 ./general_dataset/suit2F-25GLA-75INT_StartGroup%d.txt Time11' %start
				print >> dynFp, '600 ./general_dataset/suit2F-0GLA-100INT_StartGroup%d.txt Time12' %start   #3rd completed change
				print >> dynFp, '660 ./general_dataset/suit2F-25GLA-75INT_StartGroup%d.txt Time13' %start
				print >> dynFp, '720 ./general_dataset/suit2F-50GLA-50INT_StartGroup%d.txt Time14' %start
				print >> dynFp, '780 ./general_dataset/suit2F-75GLA-25INT_StartGroup%d.txt Time15' %start
				print >> dynFp, '850 ./general_dataset/suit2F-100GLA-0INT_StartGroup%d.txt Time16' %start   #4th completed change
				print >> dynFp, '925 ./general_dataset/suit2F-75GLA-25INT_StartGroup%d.txt Time17' %start
				print >> dynFp, '1000 ./general_dataset/suit2F-50GLA-50INT_StartGroup%d.txt Time18' %start
				print >> dynFp, '1075 ./general_dataset/suit2F-25GLA-75INT_StartGroup%d.txt Time19' %start
				print >> dynFp, '1150 ./general_dataset/suit2F-0GLA-100INT_StartGroup%d.txt Time20' %start   #5th completed change
				print >> dynFp, '1240 ./general_dataset/suit2F-25GLA-75INT_StartGroup%d.txt Time21' %start
				print >> dynFp, '1330 ./general_dataset/suit2F-50GLA-50INT_StartGroup%d.txt Time22' %start
				print >> dynFp, '1420 ./general_dataset/suit2F-75GLA-25INT_StartGroup%d.txt Time23' %start
				print >> dynFp, '1500 ./general_dataset/suit2F-100GLA-0INT_StartGroup%d.txt Time24' %start   #6th completed change
				print >> dynFp, '1560 ./general_dataset/suit2F-75GLA-25INT_StartGroup%d.txt Time25' %start
				print >> dynFp, '1620 ./general_dataset/suit2F-50GLA-50INT_StartGroup%d.txt Time26' %start
				print >> dynFp, '1680 ./general_dataset/suit2F-25GLA-75INT_StartGroup%d.txt Time27' %start
				print >> dynFp, '1750 ./general_dataset/suit2F-0GLA-100INT_StartGroup%d.txt Time28' %start   #7th completed change
				dynFp.close()
		
		outfilename = '%s/test%d_%s_StartGroup%d.txt' %(cwd,(z+start),w,start)
		outfile = open(outfilename, 'w')
		infile = open(splatchefile, 'r')		
		for line in infile:
			if line.startswith('#') or line.startswith('\n'):
				pass
			elif line.startswith('PopDensityFile='):
				print >> outfile, 'PopDensityFile=./general_dataset/dens_init_%s.txt' %(Model)
			elif line.startswith('PresVegetationFile='):
				print >> outfile, 'PresVegetationFile=./general_dataset/world_cellnumbers_%s.asc' %(Model)
			elif line.startswith('RoughnessTopoFile='):
				print >> outfile, 'RoughnessTopoFile=./general_dataset/world_cellnumbers_%s.asc' %(Model)
			elif line.startswith('ArrivalCellFile='):
				print >> outfile, 'ArrivalCellFile=./general_dataset/Arrival_cell_%s_StartGroup%d.col' %(Model,start)
			elif line.startswith('SampleFile='):
				print >> outfile, 'SampleFile=./general_dataset/GeneSamples_%s.sam' %(Model)
			elif line.startswith('Veg2KFile='):
				print >> outfile, 'Veg2KFile=./general_dataset/dynamic_K_%s_StartGroup%d.txt' %(w,start)
			elif line.startswith('Veg2FFile='):	
				print >> outfile, 'Veg2FFile=./general_dataset/dynamic_F_%s_StartGroup%d.txt' %(w,start)
			elif line.startswith('GrowthRate='):
				print >> outfile, 'GrowthRate=%.5f' %(GR)
			elif line.startswith('MigrationRate='):
				print >> outfile, 'MigrationRate=%.5f' %(m)
			elif line.startswith('NumGeneticSimulations='):
				print >> outfile, 'NumGeneticSimulations=%d' %(repsGen)
			elif line.startswith('AncestralSize='):
				print >> outfile, 'AncestralSize=%d' %(maxK/5)
			else:
				print >> outfile, line.strip()
		infile.close()
		outfile.close()

	#this portion runs splatche for each parameter set as many times as demographic simulations requested 
	print '{:^80s}'.format('\n\n   NOW TESTING %s model with the following parameters:') %Model
	print '{:^80s}'.format('r: %s, m: %s, K: %s\n\n                                   ') %(par_used.split('\t')[1],par_used.split('\t')[2],par_used.split('\t')[3])
	dobreak = 'no'		#just an object to prevent from breaking this loop unless not all sampling are colonized
	for r in xrange(repsDem):
		if dobreak=='yes':
			break		
		else:
			for w in worlds:
				# RUNNING SPLATCHE + associated analyses!!!
				cmdA =deque()
				cmd0 = 'cp %s/general_dataset/Arrival_cell_%s.col %s/general_dataset/Arrival_cell_%s_StartGroup%d.col' %(cwd,Model,cwd,Model,start)
				cmdA.append(shlex.split(cmd0))
				exeRun = '%s/splatche2_StartGroup%d' %(cwd,start)
				if exeRun not in os.listdir(cwd):
					cmd1 = 'cp %s/splatche2-01_lin_64 %s' %(cwd,exeRun)
					cmdA.append(shlex.split(cmd1))
				outfilenameRun = '%s/test%d_%s_StartGroup%d.txt' %(cwd,(z+start),w,start)
				cmd2 = '%s %s' %(exeRun, outfilenameRun)
				cmdA.append(shlex.split(cmd2))
				starttime = time.time()
				for cmd in (cmdA):
					job = subprocess.call(cmd)

					# MOVING SPLATCHE outputs after verifying there are no issues !!!
				Arrivalfile = '%s/general_dataset/Arrival_cell_%s_StartGroup%d_output.txt' %(cwd,Model,start)	#arrival outputfile
				Arrivalfilepath = '%s/general_dataset' %cwd
				par_used_uncolonized_reps = '%s\tDemoRep %d\t%s\tFAILED\tFAILED\tFAILED' %(w,r,par_used)
				fileending = '_GeneSamples_%s_1.arp' %Model
				firstfile = outfilenameRun.split('/')[-1].replace('.txt',fileending)
				firstfilepath = '%s/general_dataset/GeneticsOutput' %cwd
				firstfile = '%s/%s' %(firstfilepath,firstfile)
				if os.path.getsize(Arrivalfile) == 0 or Arrivalfile.split('/')[-1] not in os.listdir(Arrivalfilepath):		#break simulations where an arrival output file has not been generated
					print >> par, par_used_uncolonized_reps
					dobreak = 'yes'
					print '\n   SIMULATION LOOP STOPPED: Arrival file not generated\n'
					break
				elif os.path.getsize(firstfile) == 0 or firstfile.split('/')[-1] not in os.listdir(firstfilepath):		#also break simulations where a genetic output has not been simulated
							print >> par, par_used_uncolonized_reps
							dobreak = 'yes'
							print '\n   SIMULATION LOOP STOPPED: Genetic .arp file not generated\n'
							break
				else:
					ArrivalTimes = arrivalreader(Arrivalfile)
					if ArrivalTimes.get('minArrival')==-1:
						print >> par, par_used_uncolonized_reps
						dobreak = 'yes'
						print '\n   SIMULATION LOOP STOPPED: Not all samples were colonized\n'
						break
					else:
						par_used_and_time = '%s\tDemoRep %d\t%s\t%d\t%d\t%.2f' %(w,r,par_used,ArrivalTimes['minArrival'],ArrivalTimes['maxArrival'],ArrivalTimes['medianArrival'])
						print >> par, par_used_and_time			
						fol = '%s/GeneticsOutputs/GeneticOutput_test%d_%s_rep%d' %(cwd,(z+start),w,r)
						cmdB = deque()
						cmd3 = 'mkdir %s/' %(fol)
						cmdB.append(shlex.split(cmd3))
						cmd4 = 'find %s/general_dataset/GeneticsOutput -name test%d_%s_StartGroup%d_GeneSamples_%s_* -exec mv -u {} %s \;' %(cwd,(z+start),w,start,Model,fol)
						cmdB.append(shlex.split(cmd4))
						for cmd in (cmdB):
							job = subprocess.call(cmd)
						
						# EXTRACTING snps from SPLATCHE outputfiles
						cmdC = deque()
						cmd5 = 'python %s/arlfile_SNPs_extract.py -p %d -d %s' %(cwd, Npops, fol)
						cmdC.append(shlex.split(cmd5))
						for nfile in os.listdir(fol):
							if nfile.endswith('.arp'):
								startgr = 'StartGroup%d' %start
								toreplace = '%s_GeneSamples_%s' %(startgr, Model)
								repnum = 'GeneSamples_%s_%s_rep%d' %(startgr, Model, r)
								cmd6 = 'mv %s/%s %s/GeneticsOutputs/%s' %(fol, nfile.replace('.arp', '.snp'), cwd, nfile.replace('.arp', '.snp').replace(toreplace, repnum))
								cmdC.append(shlex.split(cmd6))
						for cmd in cmdC:
							job = subprocess.call(cmd)
						
						# RUNNING arlsumstats on the genetic files from SPLATCHE and ERASING replicate-specific temporary files
						cmdD = deque()
						arlsumfolder = '%s/GeneticsOutputs/arlsumstat_files' %cwd
						for x in os.listdir(arlsumfolder):
							cmd8 = 'cp %s/%s %s/%s' %(arlsumfolder,x,fol,x)
							job = subprocess.call(shlex.split(cmd8))
						os.chdir(fol)		#this is necesary because subprocess doesn't run the cd command properly unless shell=True
						cmd10 = '%s/LaunchArlSumStat.sh' %(fol)
						cmdD.append(shlex.split(cmd10))
						cmd11 = 'mv %s/outSumStats.txt %s/GeneticsOutputs/outSumStats_test%d_%s_StartGroup%d_%s_rep%d.txt' %(fol,cwd,(z+start),w,start,Model,r)
						cmdD.append(shlex.split(cmd11))
						cmd12 = 'sed -i "s/$/test%d_%s_StartGroup%d_%s_rep%d\t/" %s/GeneticsOutputs/outSumStats_test%d_%s_StartGroup%d_%s_rep%d.txt' %((z+start),w,start,Model,r,cwd,(z+start),w,start,Model,r)
						cmdD.append(shlex.split(cmd12))
						for cmd in cmdD:
							job = subprocess.call(cmd)
						os.chdir(cwd)

				print '{:@^70s}'.format("TEST %d REPLICATE %d %s DONE!") %((z+start),r,w)
				print '{:^70s}'.format('Running time: %f\n\n') %(time.time()-starttime)
				sys.stdout.flush()
				if dobreak=='yes':
					print '{:!^80s}'.format('ANALYSES SKIPPED')
					print '{:^80s}'.format('Not all sampling locations were colonized under this parameter set\n')
					break
	
	outfilename2 = outfilename.replace('nopulse','pulse')
	toerase = [outfilename,outfilename2]
	for outf in toerase:
		if outf.split('/')[-1] in os.listdir(cwd):
			os.remove(outf)
	toerase2 = [K_in_file,K_out_file]
	for outf2 in toerase2:
		dirf = '%s/general_dataset' %(cwd)
		if outf2.split('/') in os.listdir(dirf):
			os.remove(outf2)
par.close()
os.remove(exeRun)

#this portion runs the 'splatchesims_condense.py' script to erase the unnecessary files, combine the independent runs' files and calculate the SSS
print
print '{:-^80s}'.format(">>>> Condensing files and calculating spatial stats <<<<")
FINALcmd = 'python splatchesims_condense.py %s %s no %d %s general_dataset/deme_coords.csv 1' %(Npops, repsDem, start, Model)
FINALcmd_return = subprocess.call(shlex.split(FINALcmd))
print '\n\n'
print '{:*^80s}'.format(" StartGroup%d Runs finished ") %start

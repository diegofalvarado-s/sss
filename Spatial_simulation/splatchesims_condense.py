#usr/bin/env python

'''
this script basically cleans out all the remaining files from the splatche runs (see 'splatchesims_run.py' script) and calls the 'summary_sumstats_splatche.R' R script to calculate spatial summary stats and generate some basic graphs.
Usage: python splatchesims_condense.py 7 10 no 0 M1 general_dataset/deme_coords.csv 4
Diego F. Alvarado-S.
Mar 16, 2014
'''

import sys, os, subprocess, shlex, shutil

numpop = int(sys.argv[1])	#defines the number of sampling populations used in the analysis
repsDem = int(sys.argv[2])	#defines the number of demographic replicates per scenario
PDF = sys.argv[3]		#whether scatterplot pdfs should be performed
start = int(sys.argv[4])	#defines the start integer for numbering simulations output
Model = sys.argv[5]		#defines which model is being run (i.e., center-in/center-out and northward/southward simulations)
coordfile = sys.argv[6]		#defines the path to the sapmples' coordinates csv-file
nproc = int(sys.argv[7])	#defines the number of processors to use
worlds = ['pulse','nopulse']
cwd = os.getcwd()
workpath = '%s/GeneticsOutputs' %(cwd)

outfiles = []
SSSset1_files = []
for filename in os.listdir(workpath):
	#this portion moves all replicates from simulation scenarios that include at leats one simulation where not all populations were colonized to a different folder so that they don't get analyzed
	if 'SpatialSumStats_SET1_' in filename:
		SSSset1_files.append(filename)
	elif 'outSumStats_test' in filename:
		if int(filename.split('_')[1].replace('test','')) in xrange(start,(start+repsDem)):
			outfiles.append(filename)

removefilename = '%s/NotRunOutfiles/removed_scenarios_StartGroup%d.txt' %(workpath, start)
workdir2 = '%s/NotRunOutfiles' %workpath
if not os.path.exists(workdir2):
	os.makedirs(workdir2)
outfile2 = open(removefilename, 'w')

#removing output files of incomplete or uncolonized runs
replic = []
removefiles = []
for f in outfiles:
	reps = f.split('_')[1]
	replic.append(reps)
	path1 = '%s/GeneticsOutputs/%s' %(cwd,f)
	currentfile = open(path1, 'r')
	tt = 0
	for line in currentfile:
		tt +=1
		if tt == 1:
			cols = line.strip().split('\t')
	currentfile.close()
	statnums = 0
	for col in cols:
		if col.startswith('K_'):		#here I'm using K (one of the stats calculated) just to count that the outputfile has sumstats for all populations have been generated
			statnums +=1
	if os.path.getsize(path1) == 0:
		reason1 = '%s: outputfile is empty' %reps
		print >> outfile2, reason1
		removefiles.append(reps)
	elif statnums != numpop:
		reason2 = '%s: not all %d populations have data' %(reps,numpop)
		print >> outfile2, reason2
		removefiles.append(reps)
	
import collections as col
freq = col.Counter(replic)
for rep in freq:
	if freq.get(rep) !=(repsDem*len(worlds)):
		reason3 = '%s: not all replicates were run' %rep
		print >> outfile2, reason3
		removefiles.append(rep)
for f in outfiles:
	if f.split('_')[1] in removefiles:
		cmd1 = 'mv %s/GeneticsOutputs/%s %s/GeneticsOutputs/NotRunOutfiles/%s' %(cwd,f,cwd,f)
		cmd1_return = subprocess.call(shlex.split(cmd1))
outfile2.close()

for f in SSSset1_files:
	if f.split('_')[1] in removefiles:
		cmd2 = 'mv %s/GeneticsOutputs/%s %s/GeneticsOutputs/NotRunOutfiles/%s' %(cwd,f,cwd,f)
		cmd2_return = subprocess.call(shlex.split(cmd2))

#this portion removes all the output files that remain there because of a bug in the 'splatchesims_run.py' script that prevents them from being erased if the runs are stopped
parfiles = []
for filename in os.listdir(workpath):
	if 'GeneticOutput_test' in filename:
		if int(filename.split('_')[1].replace('test','')) in xrange(start,(start+repsDem)):
			cmd3 = 'rm -r %s/GeneticsOutputs/%s' %(cwd,filename)
			cmd3_return = subprocess.call(shlex.split(cmd3))

	elif 'parameters_used' in filename:
		for S in xrange(start,(start+repsDem)):
			startgroup = 'StartGroup%d' %S
			if startgroup in filename:
				parfiles.append(filename)

#this portion removes all the run specific files
inputpath = '%s/general_dataset' %(cwd)
for filename in os.listdir(inputpath):
	for S in xrange(start,(start+repsDem)):
		startgroup = 'StartGroup%d' %S 
		if startgroup in filename:
			cmd4 = 'rm -r %s/general_dataset/%s' %(cwd,filename)
			cmd4_return = subprocess.call(shlex.split(cmd4))

#this portion combines all files from the separate runs for analysis with R
#first the parameter files:
combparfiles = '%s/CombinedParameterUsed_StartGroup%d.tsv' %(workpath,start)
outfile3 = open(combparfiles, 'w')
k = 0
for parfile in parfiles:
	k += 1
	parfilepath = '%s/%s' %(workpath,parfile)
	Parfile = open(parfilepath, 'r')
	l = 0
	for line in Parfile:
		l += 1
		if k == 1:
			print >> outfile3, line.strip()
		else:	#this lines prevents the header to be pasted multiple times
			if l != 1:
				print >> outfile3, line.strip()
	Parfile.close()
	os.remove(parfilepath)
outfile3.close()


#second the log files:
comblogfile = '%s/Combinedlog_StartGroup%d.log' %(cwd,start)
outfile4 = open(comblogfile, 'w')
for logfile in os.listdir(cwd):
	for S in xrange(start,(start+repsDem)):
		startgroup = 'StartGroup%d' %S 
		if logfile.startswith(startgroup):
			logfilepath = '%s/%s' %(cwd,logfile)
			Logfile = open(logfilepath, 'r')
			for line in Logfile:
				print >> outfile4, line.strip()
			Logfile.close()
			os.remove(logfilepath)
outfile4.close() 

#The following portion runs the R scripts designed to calculate spatially explicit summary stats on the previously calculated non-spatial sum stats
cwdup = '/'.join(cwd.split('/')[:-1])
cmd8 = "Rscript SSS_calculation.R GeneticsOutputs %s %d %d"	%(coordfile, nproc, start) 	#this last line estimates the complete set of spatial SumStats for each scenario for which none runs had uncolonized populations
cmd8_return = subprocess.call(shlex.split(cmd8))

#Finally, remove all remaining temporary files
for f in os.listdir(workpath):
	if f.startswith('outSumStats') or f.endswith('.snp'):
		startgrp = 'StartGroup%d' %start
		if startgrp in f:
			torem = '%s/%s' %(workpath, f)
			os.remove(torem)
	elif f.endswith('SSS-temp.tsv'):
		grp = 'Group%d'
		if startgrp in f:
			torem = '%s/%s' %(workpath, f)
			os.remove(torem)
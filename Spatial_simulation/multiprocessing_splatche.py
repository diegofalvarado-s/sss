#!/usr/bin/env python
'''Simple example program of one way to run multiple subprocesses from Python.
Modified from the cmdForLoop.py script of Jeffrey R. de Wet (see below)
Usage: python multiprocessing_splatche.py -J 12 -P 4 -r 20 -M M1 --rD 10 --rG 10
Diego F. Alvarado
Dec, 23, 2013

    ---------
    A double-ended queue is used to store running subprocess and a separate deque
    has the command arguments for jobs waiting to be run. As a job finishes, it is
    removed from the deque and processed. If there are still jobs waiting, one
    is removed from the wait list, started as a new subprocess, and added to the
    active jobs deque. If the end job in the active jobs deque is not ready for
    processing, the jobs are rotated in the deque. runForLoop.py (used as the
    executable that this program runs as subprocesses) must be in the same
    directory as this program in order for it to run.
    
    Jeffrey R. de Wet
    University of Michigan
    02/24/2012
    ---------
    
'''

import time, subprocess, random, sys, os, shlex, shutil
from collections import deque
from optparse import OptionParser

usage = "usage: %prog [options] arg"
parser = OptionParser(usage=usage)
parser.add_option("-J",dest="Jobs", help="number of replicate jobs to run")
parser.add_option("-P",dest="cores", help="number of processors to used")
parser.add_option("-r",dest="reps", help="number of scenario pairs per run")
parser.add_option("-M",dest="Model", help="type of geographic scenario; M1 for 'Up-Down mountain model' or M2 for 'South-North model'")
parser.add_option("--rD",dest="Demoreps", default=10, help="number of demographic replicates per scenario")
parser.add_option("--rG",dest="Genreps", default=10, help="number of genetic replicates per scenario")
parser.add_option("--np",dest="NumPops", default=7, help="number of samplig populations in SPLATCHE")
(options,args) = parser.parse_args() 

numJobs = int(options.Jobs)	#defines the number of replicates to run
maxActiveJobs = int(options.cores)	#defines the number of processors to be used
reps = options.reps		#defines the number of scenario pairs per run
Model = options.Model		#defines whether a "Up-Down mountain model" (named M1) or "South-North model" (named M2) is to be run
Demrep = options.Demoreps	#defines the number of demographic replicates per scenario
Genrep = options.Genreps	#defines the number of genetic replicates per scenario
numpops = options.NumPops  #number of samplig populations in SPLATCHE
checkJobInterval = int(Demrep)*15.0	#defines how often to check if a process is done; in this case I calculate that each run takes about 15 seconds, but it could be adjusted according to the specific SPLATCHE simulation time


#bring in the spatial stats R scripts
shutil.copyfile('../SSS_calculation/SSS_calculation.R', './SSS_calculation.R')
shutil.copyfile('../SSS_calculation/spatial_sumstats_functions.R', './spatial_sumstats_functions.R')

cmd = os.getcwd() + '/splatchesims_run.py'
waitingJobs = deque()
for jobNum  in xrange(numJobs):
    n = (jobNum*(int(reps)+int(Demrep)))+1
    waitingJobs.append([cmd, '-M', Model, '-s', reps, '--rD', Demrep, '--rG', Genrep, '--N', str(n)])

activeJobs = deque()
if len(waitingJobs) < maxActiveJobs:
	jobsToStart = len(waitingJobs)
else:
	jobsToStart = maxActiveJobs

print "Job manager started at %s" % time.asctime()
print "Maximum %d concurrent jobs.\n" % maxActiveJobs
startTime = time.time()
#start the max number of jobs
for x in xrange(jobsToStart):
    cmd = waitingJobs.popleft()
    print '{:~^80s}'.format(" Now Running: ")
    print ' '.join(cmd) + '\n'
    loggingfile = 'StartGroup_%s.log' % cmd[-1]
    logfile = open(loggingfile, 'w')
    newJob = subprocess.Popen(cmd, stdout=logfile)
    activeJobs.append(newJob)
    logfile.close()

print '%d jobs active, %d jobs waiting.\n' % (len(activeJobs), len(waitingJobs))

#manage active jobs and start new ones as space becomes available
while activeJobs:
    #add new job to activeJobs if there is room and there is a waiting job
    if (len(activeJobs) < maxActiveJobs) and waitingJobs:
        cmd = waitingJobs.popleft()
        print '{:~^80s}'.format(" Now Running: ")
        print ' '.join(cmd) + '\n'
        loggingfile = 'StartGroup_%s.log' % cmd[-1]
        logfile = open(loggingfile, 'w')
        newJob = subprocess.Popen(cmd, stdout=logfile)
        activeJobs.appendleft(newJob)
        logfile.close()
    #check on a job to see if it is ready
    returnCode = activeJobs[0].poll() #return code will be None if still running
    if returnCode is not None:
        finishedJob = activeJobs.popleft()
    else:
        activeJobs.rotate(-1)
    #print 'Number of active jobs:', len(activeJobs)
    time.sleep(checkJobInterval)


#condense individual StartGroup files
if len(activeJobs) == 0:
    print
    print '{:~^80s}'.format(">>>> Condensing StartGroup files <<<<")

    workdir = os.getcwd()
    logfile = '%s/GeneticsOutputs/CombinedLogFile.log' %workdir
    outf_L = open(logfile, 'w')
    for filename in os.listdir(workdir):
        if filename.startswith('Combinedlog_StartGroup'):
            logfilepath = '%s/%s' %(workdir,filename)
            Logfile = open(logfilepath, 'r')
            for line in Logfile:
                print >> outf_L, line.strip()
            Logfile.close()
            os.remove(logfilepath)
    outf_L.close() 
    
    workdir1 = '%s/GeneticsOutputs' %(workdir)
    combparfile = '%s/CombinedParameterUsed.tsv' %(workdir1)
    outf_C = open(combparfile, 'w')
    k = 0
    for filename in os.listdir(workdir1):
        if filename.startswith('CombinedParameterUsed_StartGroup'):
            k += 1
            parfilepath = '%s/%s' %(workdir1,filename)
            Parfile = open(parfilepath, 'r')
            l = 0
            for line in Parfile:
                l += 1
                if k == 1:
                    print >> outf_C, line.strip()
                else:	#this lines prevents the header to be pasted multiple times
                    if l != 1:
                        print >> outf_C, line.strip()
            Parfile.close()
            os.remove(parfilepath)
    outf_C.close()
    
    workdir2 = '%s/NotRunOutfiles' %workdir1
    if not os.path.exists(workdir2):
	os.makedirs(workdir2)

    removefile = '%s/RemovedFiles.txt' %(workdir2)
    outf_R = open(removefile, 'w')
    for filename in os.listdir(workdir2):
        if filename.startswith('removed_scenarios_StartGroup'):
            removfilepath = '%s/%s' %(workdir2,filename)
            Removfile = open(removfilepath, 'r')
            removedtests = []
            for line in Removfile:
                if line not in removedtests:        #this way only unique combinaations of parameters are reported
                    removedtests.append(line.strip())
            for test in removedtests:
                print >> outf_R, line.strip()
            os.remove(removfilepath)
    outf_C.close()

    #remove the left out original folder where splatche saved output
    CMD = 'rm -r %s/general_dataset/GeneticsOutput' %os.getcwd()    
    os.system(CMD)

    #remove the spatial stats R scripts
    os.remove('./SSS_calculation.R')
    os.remove('./spatial_sumstats_functions.R')
    
    print '\n\n'
    print '{:*^80s}'.format(" Main Program Finished ")
    print 'Elapsed time %f\n' % (time.time() - startTime)
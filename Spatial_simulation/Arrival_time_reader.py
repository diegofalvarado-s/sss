'''
this simple script just reads the Arrival ouputfile of splatche to extract arrival times and to assess whether all sampling populations have been colonized. It outputs a warning if this is not the case
Diego F. Alvarado-S., Nov 14, 2013
'''

def arrivalreader(wd):
	import numpy as np
	infile = open(wd, 'r')
	arrivalT = []	#stores arrival time for all sampling locations
	n = 0
	for line in infile:
		n +=1	
		if n>1:
			value = int(line.strip().split(' : ')[1])
			arrivalT.append(value)
	Arrival = {'minArrival': min(arrivalT), 'maxArrival': max(arrivalT), 'medianArrival': np.median(arrivalT)}
	return(Arrival)
